#!/usr/bin/env python3
# pylint: disable=trailing-newlines,missing-docstring,invalid-name
'''
extract current cluster name + kube namespace for inclusion in a CLI prompt
or something

'''

import os
import sys
import yaml

__context__ = {}


def _home():
    return os.environ.get('HOME', '/tmp')


def _configpath():
    '''
    . check KUBECONFIG environment variable
    . if what it refers to exists, return its value
    . if not, ignore it and check the default location
    . if no file exists at the default location, return None

    '''

    if __context__.get('kctl_config_path', None):
        retval = __context__['kctl_config_path']
    else:
        retval = None

        kctl_env = os.environ.get('KUBECONFIG', '')
        kctl_default = os.path.join(_home(), '.kube', 'config')

        if os.path.exists(kctl_env):
            retval = kctl_env
        else:
            if os.path.exists(kctl_default):
                retval = kctl_default

        __context__['kctl_config_path'] = retval

    return retval


def read_config():
    '''

    read kubectl config, deserialize and return python structure

    '''

    retval = None

    if __context__.get('kctl_config', None):
        retval = __context__['kctl_config']
    else:
        if _configpath():
            with open(_configpath()) as fh:
                kctl_config = yaml.load(fh)

            retval = kctl_config

    return retval


def _current_context():
    retval = read_config().get('current-context', None) if read_config() else None
    return retval


def current_context_data():

    contexts = read_config().get('contexts', [])
    current = _current_context()

    retval = [
        ctx['context']
        for ctx in contexts
        if ctx['name'] == current
    ].pop()

    return retval


def get_elements():
    '''

    takes one argument so we're going ghetto and grabbing the last element
    of sys.argv

    '''

    retval = None
    current = current_context_data()
    specific = sys.argv[-1]

    if specific not in current:
        bits = current
    else:
        bits = {specific: current[specific]}

    for k, v in bits.items():
        print('export K8S_{}="{}"'.format(k.upper(), v))

    return retval


if __name__ == "__main__":
    get_elements()
