# bash-k8s-prompt

## What it is

shell bits to add the currently configured kubectl context to a bash
command prompt

## Why does it exist?

The only ways to get an idea of what kubernetes context your unqualified 
`kubectl` or `helm` command are flung towards are:

  1. Read `~/.kube/config` (yay, YAML!)
  1. `kubectl config get-contexts`


```
CURRENT   NAME                             CLUSTER                          AUTHINFO                                NAMESPACE
*         cluster-dev                      cluster-dev                      k2-2.dev                                kube-system
          cluster-prod                     cluster-prod                     k1.prod.somedomain.com-basic-auth       roe
          wheeee                           cluster-prod                     k1.prod.somedomain.com-basic-auth       kube-system
          dev-cluster.somedomain.com       dev-cluster.somedomain.com       dev-cluster.somedomain.com
          k1.prod.somedomain.com           k1.prod.somedomain.com           k1.prod.somedomain.com
          k2-2.dev.somedomain.com          k2-2.dev.somedomain.com          k2-2.dev.somedomain.com                 roe
          revsys                           revsys                           revsys                                  tedx
```

So, the information exists... inconveniently. When hopping around between tasks on different clusters,
within different namespaces, it becomes critical to know what the command target is quickly. This 
avoids inadvertently deploying the wrong thing (or, perhaps, the right thing in the wrong
spot). This scenario is exacerbated by the erm... Lack of Input By a Qualified UX
Engineer with regards to the `kubectl` utility.  

----
Sidebar

`kubectl config use-context <context-name>` sets the values in the `current-context`
dictionary in `~/.kube/config`.

To adjust the other values of the context requires the use of `kubectl config set-context <context-name> --namespace=<whatever> --user=ME`

_set_ vs. _use_.  Ok.  Well, `use-config` will accept `--namespace` and/or `--user` but 
they are thrown away.  If, in a fit of banging out dozens of `kubectl` commands per
minute you *think* you’re on cluster X within namespace Y, you might really still
be on the previous cluster in a production namespace deploying your game server 
pod.

Needless to say, since we’re not machines (yet), it is quite beneficial to have 
those small, yet important pieces of information Right There In Front of You.

----

`kube_context.py` merely parses the kubenetes config file and provides environment
variables that can be plugged in with shell fu.


This short bash function get plugged into the special `PROMPT_COMMAND` bash
variable, so issuing `kubectl config set-context <other context name> --namespace=whee`
will update the prompt immediately.

## How to do it

```

#: cp k8s-prompt ~/.k8s-prompt

#: echo ‘. ~/.k8s-prompt’ >> ~/.bashrc

#: sudo cp bin/kube_context.py /usr/local/bin/.

#: kube_context.py
export K8S_CLUSTER="k2-2.dev.revsys.com"
export K8S_NAMESPACE="kube-system"
export K8S_USER="k2-2.dev"

```

These variables get injested by `.k8s-prompt` and turned into a prompt.  

----

**NOTE**

My prompt-fu is not mighty. Consider this a proof of concept and fiddle as you will.

----

What this repository provides gives the following prompt:

```

[[context-name:namespace]]
/current/path
user@host: 

```

It is worthwhile to note that in most cases the context name happens to also 
be the cluster name.


## `KUBECONFIG` env variable

This variable is used by both `helm` and `kubectl` to allow the user to specify 
an alternate location for a kubernetes configuration file.  `kube_context.py` 
looks for this variable. If it exists and points to a file that also exists, the
prompt will be updated to reflect the current-context configured within that
file. If the variable exists but does not point to an actual file, it will fall 
back to the default location (`~/.kube/config`)

# Questions?

If you have a question about this project, please open a GitLab issue. If you love us and want to keep track of our goings-on, here's where you can find us online:

<a href="https://revsys.com?utm_medium=gitlab&utm_source=bash-k8s-prompt"><img src="https://pbs.twimg.com/profile_images/915928618840285185/sUdRGIn1_400x400.jpg" height="50" /></a>
<a href="https://twitter.com/revsys"><img src="https://cdn1.iconfinder.com/data/icons/new_twitter_icon/256/bird_twitter_new_simple.png" height="43" /></a>
<a href="https://www.facebook.com/revsysllc/"><img src="https://cdn3.iconfinder.com/data/icons/picons-social/57/06-facebook-512.png" height="50" /></a>
<a href="https://github.com/revsys/"><img src="https://assets-cdn.github.com/images/modules/logos_page/GitHub-Mark.png" height="53" /></a>
<a href="https://gitlab.com/revsys"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/2000px-GitLab_Logo.svg.png" height="44" /></a>
